//
//  main.cpp
// 
//
//  Created by Andre White on 2/17/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

// Assignment 1: Alternating disks problem, left-to-right algorithm
// Given 2n alternating disks (dark, light) the program reads the number of single color disks
// (light or dark), arranges the disks in the correct order and outputs the number of swaps
// INPUT: a positive integer n and a list of 2n disks of alternating colors dark-light, starting with dark
// OUTPUT: a list of 2n disks, the first n disks are light, the next n disks are dark,
// and an integer m representing the number of moves to move the dark ones after the light ones

#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;

// function to print the list of disks, given the number of single color disks and the actual list
// n represents the number of single color disks
// disks represents the list of disks (index 0 being the first disk) where
// 0 = a light color disks
// 1 = a dark color disks
void print_disks(int n, char *disks){
    for (int x=0; x<n; x++) {
        printf("%s",disks[x]?"d":"l");
    }
    printf("\n");
}
//algorithm goes from left to right swapping disks if consecutive disks are alternating in the form dl

int leftToRight(char* disks, int n){
    int numOfSwaps=0;
    // loop to push light one before the darks ones
    for (int k=1;  k <= n ; k++) {
        for (int i=0; i<2*n-k; i++) {
            if (disks[i]==1&&disks[i+1]==0) {
                //swap with xor
                disks[i]=disks[i] xor disks[i+1];
                disks[i+1]=disks[i] xor disks[i+1];
                disks[i]=disks[i] xor disks[i+1];
                numOfSwaps++;
            }
        }
    }
    return numOfSwaps;
}
//algorithm goes from left to right swapping disks like the leftToRight algorithm, then goes from right to left doing the same thing similar to how one uses a lawnmower to cut grass.
int lawnmower(char* disks, int n){
    int numOfSwaps=0;
    for (int k=1;  k <= n/2+1 ; k++) {
        // Going left to right
        for (int i=0; i<2*n-k; i++) {
            if (disks[i]==1&&disks[i+1]==0) {
                //swap with xor
                disks[i]=disks[i] xor disks[i+1];
                disks[i+1]=disks[i] xor disks[i+1];
                disks[i]=disks[i] xor disks[i+1];
                numOfSwaps++;
            }
        }
        // Going right to left
        for (int y=2*n-k;y>0 ; y--) {
            if (disks[y]==0&&disks[y-1]==1) {
                //swap with xor
                disks[y]=disks[y] xor disks[y-1];
                disks[y-1]=disks[y] xor disks[y-1];
                disks[y]=disks[y] xor disks[y-1];
                numOfSwaps++;
            }
        }
    }
    return numOfSwaps;
    
}
void prompt(){
    int n, numOfSwaps;
    char *disks;
    // display the header
    cout << "Enter the number of single color disks (light or dark)" << endl;
    // read the number of disks
    cin >> n;
    // allocate space for the disks
    disks = new char[2*n];
    // set the initial configurations for the disks to alternate
    for(int i=0; i < n; i++) {
        disks[2*i] = 1;
        disks[2*i+1] = 0;
    }
    
    // print the initial configuration of the list of disks
    cout << "Initial configuration" << endl;
    print_disks(n*2,disks);
    numOfSwaps=0;
    int choice=-1;
    //prompt for algorithm choice
    while (choice<0||choice>1) {
        cout<<"Choose Algorithm to use: 1 for left to right or 0 for lawnmower"<<endl;
        cin>>choice;
    }
    //calculate number of swaps based on the algorithm choice
    numOfSwaps=choice?leftToRight(disks, n):lawnmower(disks, n);
    
    // after shuffling them
    cout << "After moving darker ones to the right" << endl;
    print_disks(n*2, disks);
    // print the total number of moves
    cout << "Number of swaps is " << numOfSwaps << endl;
    // de-allocate the dynamic memory space
    delete [] disks;
}

int main(int argc, const char * argv[])  {
    char answer='Y';
    cout  << "Alternating Disks Problem" << endl;
    while (toupper(answer)=='Y') {
        prompt();
        cout<<"Try again?(Y/N)"<<endl;
        cin>>answer;
        //make sure answer is y or n
        while (toupper(answer)!='Y'&&toupper(answer)!='N') {
            cout<<"Try again?(Y/N)"<<endl;
            cin>>answer;
        }
    }
    
    return EXIT_SUCCESS;
}


